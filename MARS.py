# MARS - Microbial Abundances Retrieved from Sequencing data
# Created by: 10/12/21    - DATE
# Author: Tim Hulshof & Bram Nap
# Programmed with python 3.10 (any other dependencies we should include versions of?)

# Import statements
import pandas as pd
import numpy as np
import os
import time
from datetime import datetime
import platform
import requests


# Function user interacts with. Calls upon the entire MARS pipeline
def MARS(path_to_feature_table, path_to_taxonomy_table, output_folder_name="output", wgs=False, bound=0,
         alt=False, TOI=None):
    # TODO Some detailed description of MARS
    # Input:    path_to_feature_table:  xx
    #           path_to_taxonomy_table: xx
    #           output_folder_name:     Name of the folder where results are stored. Default is "output"
    #           wgs:                    States if the data is whole-genome shotgun. Default is False
    #           bound:                  xx
    #           alt:                    States if the input file is of an alternative data structure. Default is False
    #           TOI:                    Taxonomical levels of interest. Input should be a list. Default is an empty list
    #                                   and functionality will not be performed
    # Output:   overview:
    #           coverage:
    #           absent_present_species
    #           absent_present_genera
    #           agora2_species_normed
    #           agora2_genera_normed
    #           agora2_mixed_normed
    
    """Microbial Abundances Retrieved from Sequencing data"""
    
    # Deals with naming conventions if the name does not follow "__"/"; "
    def naming_convention_handler(df):
        # Function to acquire the correct naming conventions #TODO is this for agora or the input file or both?
        # Input:    df: A dataframe which has taxonomic information
        # Output    df: The same dataframe as the input but with altered taxonomic naming conventions

        df = df.replace("\[|\]", "", regex=True)  # considers bracketed entries
        df = df.replace(to_replace="/\_(?!\_)/", value='__', regex=True)  # if _ is not followed by another _
        df = df.replace(to_replace=";(?! )", value='; ', regex=True)  # if ; is not followed by " "
        df = df.replace(to_replace=" sp ", value=' sp. ', regex=True) # Adds '.'
        
        return df
    
    # Splits taxonomic string
    # TODO annotate this function
    def splitter_operations(df, wgs, tax_split=False):
        # Function to split taxonomic information from the raw input file on species and genus level. NOTE: Requires a
        # Pandas Series as input df #TODO maybe alter df from input argrument to Series?
        # Input:    df:         A Pandas Series containing taxonomic information
        #           wgs:        Carry over from the main MARS call. States if data is wgs or not
        #           tax_split:  Boolean to indicate if the taxa should be split or not, defaults to False
        # TODO counterintuitive to have a boolean to say if you want to split or not in a split function.
        # Output:   genera_and_species[0]: ...
        #           genera:
        #           species:
        #           species_to_genera_dict:

        # Splits the string on "; g__", one time and creates a df with col1 taxa info higher than genus level and col2
        # The genus and species information
        genera_and_species = df.str.split("; g__", n=1, expand=True)

        # Checks if further processing of the data is required
        if tax_split:
            # if True, Returns all taxa info available on a higher level then genus
            return genera_and_species[0]
        else:
            # if False, drops the None (failed splits indicating no information on genus level) and continues with only
            # genus and species (if available) data
            genera_and_species = genera_and_species.dropna()
            genera_and_species = genera_and_species[1]

            if wgs:
                # If the data is wgs, separate taxonomic info in whether they contain species info
                species = genera_and_species[~genera_and_species.str.contains("; s__$", regex=True)]
                genera = genera_and_species[genera_and_species.str.contains("; s__$", regex=True)]

                # Create a dictionary where all the species in the dataset are matched to their genus
                species_to_genera = species.str.split("; s__", n=1, expand=True)
                species_to_genera = species_to_genera.set_index(1)
                species_to_genera_dict = species_to_genera.to_dict()[0]

                # Get only the species information by removing everything until s__
                species = species.replace(".*; s__", "", regex=True)

                return genera, species, species_to_genera_dict
            else:
                # If data is not wgs #TODO need some extra info on whats going on here
                genera_and_species = genera_and_species.replace(to_replace="; s__", value=' ', regex=True)
                # get rid of whitespace for genera specific entries
                genera_and_species = genera_and_species.str.rstrip()
                
                species = genera_and_species[genera_and_species.str.contains(" ", regex=True)]
                genera = genera_and_species[~genera_and_species.str.contains(" ", regex=True)]
            
                return genera, species, None
    
    # Eliminates empty tags
    # TODO annotate this function
    def empty_taxon_eliminator(df):
        # Function that removes empty taxonomic information #TODO does this also only work with Series or with whole df?
        # Input:    Dataframe with raw taxonomic information
        # Output:   Dataframe with cleaned up taxonomic information

        # Define different identifiers for the differen taxonomic levels
        #TODO class_ can be renamed to class ???
        phylum, class_, order, family, genus = "; p__", "; c__", "; o__", "; f__", "; g__"

        # If taxanomic info ends with ; x__, where x is a taxonomic identifier, remove it as it gives no info.
        df = df.replace(to_replace=genus + "$", value='', regex=True)
        df = df.replace(to_replace=family + "$", value='', regex=True)
        df = df.replace(to_replace=order + "$", value='', regex=True)
        df = df.replace(to_replace=class_ + "$", value='', regex=True)
        df = df.replace(to_replace=phylum + "$", value='', regex=True)
        
        return df
    
    # Checks if name in agora2
    # TODO annotate this function
    def agora_checker(summed, agora2, mixed=False, species_to_genus=None):
        # Function to see if the genus or species is present in the AGORA2 infofile. Here the AGORA2 is read as a api
        # Also removes species/genera not found in AGORA2 from the dataframe
        # TODO Tim specify further how AGORA2 is read here.

        # Input:    summed: #TODO no clue what summed is
        #           agora2: Information on which species/genera GEMs are available. Needs to be information on ONE
        #           taxonomical level #TODO TIM CHECK
        #           mixed:  If a mixed species/genus information should be generated, defaults to FALSE
        #           species_to_genus: #TODO TIM
        # Output:   df:         Input dataframe where the to_drop species/genera are removed #TODO change the name in the return statement
        #           to_drop:    List of species/genera not present in AGORA2
        #           present:    List of species/genera present in AGORA2

        #TODO I assume this has to do with the mixed pangenus/species models, UNSURE because Ines does not want to use this right? HOLDOFF annotation for now
        if mixed:
            to_drop = []
            present = []
            genus_to_keep = {}
            for name in summed.index:
                if " " in name:  # check if species
                    if name in agora2:
                        present.append(name)
                    else:
                        if wgs:
                            try:  # Possibility of genus getting here so need try statement e.g. "candidatus something"
                                genus = species_to_genus[name]
        #                         genus = name.split(None, 1)[0]
                                if genus in agora2:
                                    genus_to_keep[name] = genus
                                    # genus_to_keep.append(name)
                                else:
                                    to_drop.append(name)
                            except:  # TODO specify the exception
                                to_drop.append(name)
                        else:
                            genus = name.split(None, 1)[0]
                            if genus in agora2:
                                genus_to_keep[name] = genus
                                # genus_to_keep.append(name)
                            else:
                                to_drop.append(name)
                else:
                    if name in agora2:
                        present.append(name)
                    else:
                        to_drop.append(name)
                        
            return summed.drop(to_drop), to_drop, genus_to_keep         
        else:
            # Initialise two lists for species/genera present or not present in AGORA2
            to_drop = []
            present = []

            # Checks for each taxonomic name species/genus if it is present in AGORA2
            for name in summed.index:
                if name in agora2:
                    # If present in AGORA2 add to present list
                    present.append(name)
                else:
                    # If not present add to to_drop list
                    to_drop.append(name)
                
            return summed.drop(to_drop), to_drop, present

    # TODO annotate this function
    def preprocessing_alt():
        
        # Finds named genus in original file. Needed to check how species genus name and listed genus name relate
        # TODO annotate this function
        def identify_genus(entry, df, agora_name):
            genus_entry = df[df.index.str.contains(entry.replace(" ", "_"), regex=True)].copy() 
            if genus_entry.empty:
                genus_entry = df[df.index.str.contains(agora_name.replace(" ", "_"), regex=True)].copy()
            genus_entry.index = genus_entry.index.str.replace(".+?(?=(?i)g__)", "", regex=True)
            genus_entry.index = genus_entry.index.str.replace("g__", "", regex=True)
            genus_entry.index = genus_entry.index.str.replace("\|s__.*", "", regex=True)
            identified_genus = genus_entry.index[0]
            if entry in agora2_species_to_genus:
                if agora2_species_to_genus[entry] != identified_genus:
                    return agora2_species_to_genus[entry]
                else:
                    return identified_genus
            else:
                return identified_genus
        
        # Finds homosynonym genus if there
        # TODO annotate this function
        def homosynonym_genus_correction():
            homosynonym_genus = homosynonyms[taxon].split(None, 1)[0] 
            if identified_genus == homosynonym_genus:
                genera_without_species.loc[identified_genus] -= entry
            else:
                genera_without_species.loc[homosynonym_genus] -= entry
        
        df = pd.read_excel(path_to_feature_table)
        df = df.replace("\[|\]", "", regex=True)
        df = df.set_index("clade_name")
        
        species = df[df.index.str.contains("s__", regex=True)].copy()
        species.index = species.index.str.replace(".+?(?=(?i)s__)", "", regex=True)
        species.index = species.index.str.replace("s__", "", regex=True)
        species.index = species.index.str.replace("_", " ", regex=True)
        
        genera = df[df.index.str.contains("g__", regex=True)].copy()
        genera = genera[~genera.index.str.contains("s__", regex=True)]
        genera.index = genera.index.str.replace(".+?(?=(?i)g__)", "", regex=True)
        genera.index = genera.index.str.replace("g__", "", regex=True)

        # request to MARS api for homosynonyms to rename current names to agora2 names
        response_homosynonyms = requests.get(address + "homosynonyms")
        homosynonyms = response_homosynonyms.json()
        
        # request to MARS api for agora2 species genus relations
        response_agora2_species_to_genus = requests.get(address + "species_to_genus")
        agora2_species_to_genus = response_agora2_species_to_genus.json()

        species = species.rename(index=homosynonyms)
         
        for former_name in homosynonyms:
            agora_name = homosynonyms[former_name]
            agora_genus = agora_name.split(None, 1)[0]
            former_genus = former_name.split(None, 1)[0]
            if agora_name in species.index:
                entry = species.loc[agora_name]
                identified_genus = identify_genus(former_name, df, agora_name)
                if agora_genus != identified_genus:
                    if agora_genus in list(genera.index):
                        genera.loc[agora_genus] += entry
                        genera.loc[former_genus] -= entry
                    else:
                        genera.loc[former_genus] -= entry
                        genera = genera.append(entry.rename(entry.name.split(None, 1)[0]))  # renaming to just genus

        # creating mixed genus and species
        genera_without_species = genera.copy()
        key_list = list(homosynonyms.keys())
        val_list = list(homosynonyms.values())
        for taxon in species.index:
            entry = species.loc[taxon] 
            try:
                position = val_list.index(taxon)
                taxon = key_list[position]
                identified_genus = identify_genus(taxon, df, val_list[position])
            except:
                identified_genus = identify_genus(taxon, df, None)
        
            taxon_genus = taxon.split(None, 1)[0]
            # Have to see if genus is different because of homosynonym or named differently anyway
            if taxon_genus != identified_genus:
                if identified_genus in homosynonyms:
                    homosynonym_genus_correction()
                try:
                    genera_without_species.loc[identified_genus] -= entry
                except:
                    genus_entry = df[df.index.str.contains(taxon.replace(" ", "_"), regex=True)].copy() 
                    genus_entry.index = genus_entry.index.str.replace(".+?(?=(?i)g__)", "", regex=True)
                    genus_entry.index = genus_entry.index.str.replace("g__", "", regex=True)
                    genus_entry.index = genus_entry.index.str.replace("\|s__.*", "", regex=True)
                    identified_genus_old = genus_entry.index[0]  
                    if identified_genus != identified_genus_old:
                        genera_without_species.loc[identified_genus_old] -= entry
                        genera_without_species = genera_without_species.append(entry.rename(identified_genus))
                    else:
                        genera_without_species.loc[identified_genus_old] -= entry
            else:
                try:
                    homosynonym_genus_correction()
                except:
                    genera_without_species.loc[taxon_genus] -= entry
        
        genera[genera < 10**-4] = 0
        # !rounding errors when subtracting! Not ideal fix but works for now *look into if this can be corrected*
        genera_without_species[genera_without_species < 10**-4] = 0
        # gets rid of rows with just zeroes
        genera_without_species = genera_without_species[genera_without_species.any(axis=1)]
        
        frames = [genera_without_species, species]
        mixed = pd.concat(frames)

        # renaming series name
        mixed.index = mixed.index.rename("Genus and Species")
        genera.index = genera.index.rename("Genus")
        species.index = species.index.rename("Species")

        return mixed, species, genera, None, None

    # TODO annotate this function
    def preprocessing():
        # Function that runs preprocessing of the raw data to a easy readable format #TODO tim, does it also already give the final outputs used for MgPipe?
        # Input:    # TODO Tim, is it not counterintuitive to have no input statements?
        # Output:

        # Files read in/genus and species info retrieved from taxonomy table

        if path_to_taxonomy_table == "None":
            # If no taxonomy table is supplied, it is assumed that the taxonomy is already in the feature table? #TODO Tim is this correct?

            # TODO if deprecated, we should remove the bottom 3 lines
            # merged = pd.read_csv(path_to_feature_table, sep='\t', header = 1, index_col = "#OTU ID")
            # merged = merged.reset_index()
            # merged = merged.rename(columns={"#OTU ID": "Taxon"})

            # Read the taxonomy file, NOTE it should be a .tsv file/ tab delimited
            # TODO should we make it so different input data structures can be read?
            # TODO merged is a misleading variable name here, i suggest to alter it
            merged = pd.read_csv(path_to_feature_table, sep='\t')

            # TODO ID is very general, is it required to state a column with ID is required?
            tax = merged["ID"]

            # Convert the tax Series to a dataframe #TODO why tim?
            tax = tax.to_frame()
            # Replace | with ; the \ is an escape sequence required for correct reading
            # TODO not do this before you make it a df?
            tax["Taxon"] = tax.replace("\|", "; ", regex=True)

            # TODO what do you do when there is no ID named column, also why make 'm indexes?
            tax = tax.set_index("ID")
            
            feat = merged.set_index("ID")

            # Run the taxa names throught the naming_convention_handler to ensure correct naming conventions
            tax["Taxon"] = naming_convention_handler(tax["Taxon"])
            tax["Taxon"] = tax["Taxon"].replace(to_replace="_", value='__', regex=True)
            tax["Taxon"] = tax["Taxon"].replace("; t_.*", "", regex=True)
        else:
            feat = pd.read_csv(path_to_feature_table, sep='\t', header=1, index_col="#OTU ID")  # TODO(*)
            tax = pd.read_csv(path_to_taxonomy_table, sep='\t', index_col="Feature ID")  # TODO(*)
            # tax = tax[~tax["Taxon"].str.contains("\[")]

            # Run the taxa names throught the naming_convention_handler to ensure correct naming conventions
            tax["Taxon"] = naming_convention_handler(tax["Taxon"])

            #TODO what is t_ in the datafiles?
            tax["Taxon"] = tax["Taxon"].replace("; t_.*", "", regex=True)
        
        # Split the genera and species from the other taxonomic data
        genera_without_species, species, species_to_genera_dict = splitter_operations(tax["Taxon"], wgs)

        # request to MARS api for homosynonyms to rename current names to agora2 names
        response_homosynonyms = requests.get(address + "homosynonyms")
        homosynonyms = response_homosynonyms.json()
        
        # request to MARS api for agora2 species genus relations
        response_agora2_species_to_genus = requests.get(address + "species_to_genus")
        agora2_species_to_genus = response_agora2_species_to_genus.json()

        # Check if there are homosynonyms aka different names for the same genus/species
        # TODO Tim, you should walk me through whats going on in the following ~10 lines or so
        if species_to_genera_dict is not None:
            for former_name in homosynonyms:
                # TODO, How does the species_to_genera_dict look like??
                if former_name in species_to_genera_dict:
                    species_to_genera_dict[homosynonyms[former_name]] = species_to_genera_dict.pop(former_name)
                    # homosynonym genus name debatable maybe **
                    species_to_genera_dict[homosynonyms[former_name]] = homosynonyms[former_name].split(None, 1)[0]
        
            for taxon in species_to_genera_dict:
                if taxon in agora2_species_to_genus:
                    if species_to_genera_dict[taxon] != agora2_species_to_genus[taxon]:
                        species_to_genera_dict[taxon] = agora2_species_to_genus[taxon]
        
        species = species.replace(homosynonyms)
        
        # species to genera transformation
        if species_to_genera_dict is not None:
            # For every species replace with corresponding genus
            species_to_genera = species.replace(species_to_genera_dict)
            # TODO if i understand, if there is no genus it is renamed to unclassified?
            species_to_genera = species_to_genera.replace("", "Unclassified")
        else:
            # TODO this is to remove all the 'points' from the taxa names right?
            species_to_genera = species.replace(" .*", "", regex=True)

        # Add the genera without species information with the genera corresponding to species #TODO not a strong explanation
        # TODO also cannot this be done in 1 line instead of 2?
        frames = [genera_without_species, species_to_genera]
        genera = pd.concat(frames)
        
        # removing species that in actuality only have information up to genus level # TODO CAG or sp. I think is info on the species level right?
        # TODO is this specifically for the mixed genus/species if yes i would put it a bit down
        # TODO, you're not removing anything here youre appending
        genera_without_species = genera_without_species.append(species[species.str.contains(" CAG", regex=True)].
                                                               replace(species_to_genera_dict))
        genera_without_species = genera_without_species.append(species[species.str.contains(" \(", regex=True)].
                                                               replace(species_to_genera_dict))

        # Remove species that have strain denomitors in the name and no actual species name
        species = species[~species.str.contains(" CAG", regex=True)]
        species = species[~species.str.contains(" \(", regex=True)]
        
        # creating mixed genus and species
        frames = [genera_without_species, species]
        mixed = pd.concat(frames)
        
        # renaming series name
        mixed = mixed.rename("Genus and Species")
        genera = genera.rename("Genus")
        species = species.rename("Species")

        # bring genera and species back to initial tax to produce taxonomy table #TODO What?? I feel like things are going wrong here?
        genera_to_tax = "; g__" + genera
        if wgs:
            species_to_tax = "; s__" + species
        else:
            species_to_tax = species.replace("^.*?\s+", "",  regex=True)  # match everything until first whitespace
            species_to_tax = "; s__" + species_to_tax


        #TODO Tim i need help here to understand what is going on and why
        recombined_genera_and_species = pd.concat([genera_to_tax, species_to_tax], axis=1, sort=True)
        rest_of_taxonomy = splitter_operations(tax["Taxon"], wgs, tax_split=True)
        rest_of_taxonomy = rest_of_taxonomy.rename("upper_levels")
        recombination_of_taxa = recombined_genera_and_species.join(rest_of_taxonomy, how="right").fillna("")

        recombination_of_taxa["Taxon"] = recombination_of_taxa['upper_levels'] + recombination_of_taxa['Genus'] + \
            recombination_of_taxa['Species']
        taxonomy = empty_taxon_eliminator(recombination_of_taxa["Taxon"])

        # merging with feature table
        taxonomy_table = pd.merge(feat, taxonomy, left_index=True, right_index=True)
        merged_mixed = pd.merge(feat, mixed, left_index=True, right_index=True)
        merged_species = pd.merge(feat, species, left_index=True, right_index=True)
        merged_genera = pd.merge(feat, genera, left_index=True, right_index=True)

        # Full taxonomy, mixed, species and genera are grouped and summed
        taxonomy_table_summed = taxonomy_table.groupby('Taxon').sum() 
        total_reads = taxonomy_table_summed.sum()

        species_summed = merged_species.groupby('Species').sum() 
        species_reads = species_summed.sum()

        genera_summed = merged_genera.groupby('Genus').sum() 
        genera_reads = genera_summed.sum()
        
        mixed_summed = merged_mixed.groupby('Genus and Species').sum()

        return species_summed, species_reads, genera_summed, genera_reads, total_reads, taxonomy_table_summed, \
            mixed_summed, species_to_genera_dict

    # TODO annotate this function
    # Function that finds the summed reads of each specific taxa
    def taxon_reads(taxonomy_table, species_or_genus):
        # TODO why are you using a try here?
        try:
            # Check if there is a taxonomy table, if not skip this function
            if taxonomy_table is None:
                return None
        except:  # TODO Specify the exception
            # Initialise an empty dataframe
            reads_df = pd.DataFrame()

            # If we're looking a species level
            if species_or_genus == "species":
                # Obtain all the rows that have ; s__ in the index (the taxon name)
                reads_finder = taxonomy_table[taxonomy_table.index.str.contains("; s__", regex=True)]
                # Remove everything up the species level in the taxon name and alter some naming conventions
                reads_finder.index = reads_finder.index.str.replace(".+?(?=(?i)g__)", "", regex=True)
                #TODO is this bottom one necessary?
                reads_finder.index = reads_finder.index.str.replace("g__", "", regex=True)
                reads_finder.index = reads_finder.index.str.replace("; s__", " ", regex=True)
            # TODO I would specify here 'genus' to avoid potential errors when its not genus right?
            else:
                # Obtain all rows containing genus information in the index (taxon name)
                reads_finder = taxonomy_table[taxonomy_table.index.str.contains("; g__", regex=True)]
                # Remove everything in the taxon name until only genus information is left
                reads_finder.index = reads_finder.index.str.replace(".+?(?=(?i)g__)", "", regex=True)
                reads_finder.index = reads_finder.index.str.replace("g__", "", regex=True)
                reads_finder.index = reads_finder.index.str.replace("; s__.*", "", regex=True)
                # TODO why do you sum here but not with species?
                reads_finder = reads_finder.groupby('Taxon').sum()

            # TODO why is the genus summed 2, and what exactly do we get summed, the total reads for each sample?
            reads_df["sum"] = reads_finder.sum(axis=1)
            return reads_df.to_dict()["sum"]

    # TODO annotate this function
    # Function takes absent and present lists and returns a dataframe
    def absent_present_df(absent, present, reads_dic):
        absent_df = pd.DataFrame()
        absent_df['Taxon'] = absent     
        absent_df["Absent/Present"] = "AA_absent"

        present_df = pd.DataFrame()
        present_df['Taxon'] = present     
        present_df["Absent/Present"] = "AA_present"
        
        if reads_dic is not None:
            dropped_reads = []
            for taxon in absent:
                try:
                    dropped_reads.append(reads_dic[taxon])
                except:  # TODO Specify the exception
                    dropped_reads.append(np.nan)
                    
            absent_df["Summed Reads"] = dropped_reads
            
            kept_reads = []
            for taxon in present:
                try:
                    kept_reads.append(reads_dic[taxon])
                except:  # TODO Specify the exception
                    kept_reads.append(np.nan)
                
            present_df["Summed Reads"] = kept_reads
        
        frames = [absent_df, present_df]
        absent_present_df1 = pd.concat(frames)
        
        if reads_dic is not None:
            absent_present_df1["% of Total Summed Reads"] = \
                (absent_present_df1["Summed Reads"]/absent_present_df1["Summed Reads"].sum())*100
        
        return absent_present_df1

    # TODO annotate this function
    # Function which adds pan prefix and normalizes. Also returns agora2 reads
    def pan_normalize(agora2_summed):
        # Function that is used AFTER it is checked which genus/species are present in AGORA. It normalizes all the
        # genera/species in the metadata which are also present in AGORA2. It also returns the total amount of reads
        # that could be mapped onto agora, giving the coverage.
        # Input:    agora2_summed:  Metadata with as index taxon info on species/genus level and the amount of reads
        #                           per sample as columns
        # Output:   agora2_normed:  A df where for each sample the abundance of species/genera is normed to 1
        #           agora2_reads:   Total amount of reads that can be covered by AGORA2

        # Make the taxon names in the format for MgPipe and sort alphabetically.
        agora2_summed = agora2_summed.set_index(agora2_summed.index.str.replace(" ", "_", regex=True))
        agora2_summed = agora2_summed.set_index('pan' + agora2_summed.index.astype(str))
        agora2_summed = agora2_summed.sort_index()

        # For each sample sum all the reads to obtain the total amount of reads
        agora2_reads = agora2_summed.sum()

        # Create the dataframe by, for each sample, dividing the reads for a species/genus by the total amount of reads
        agora2_normed = agora2_summed.loc[:].div(agora2_reads) 

        # If a cutoff is given
        if bound > 0:
            # Set the normalized abundance of each species/genus that is smaller than the cutoff to 0
            agora2_normed[agora2_normed < bound] = 0
            agora2_normed_sum = agora2_normed.sum()
            # TODO Unsure what you're doing here tim
            df_normed_sum = pd.DataFrame()
            df_normed_sum["Normed Sum"] = agora2_normed_sum
            df_normed_sum = df_normed_sum.T
            
            df_normed_sum.to_csv(path + "/model_cutoff.csv")
        
        return agora2_normed, agora2_reads

    # TODO annotate this function
    # Check output folder for naming convention
    def nextnonexistent(f):
        fnew = f
        root, ext = os.path.splitext(f)
        i = 0
        while os.path.exists(fnew):
            i += 1
            fnew = '%s_%i%s' % (root, i, ext)
        return fnew

    # TODO annotate this function
    # Time Conversion
    def output_time(time_in_seconds):
        # Converts the time MARS took to finalise the data and converts is into a easy readable format
        # Input:    time_in_seconds:    Time MARS used to run in seconds
        # Output:   printed string:     A printed string on the console that tells the elapsed time

        # If there are only seconds
        if time_in_seconds < 60:
            return f"{str(round(time_in_seconds))} seconds"
        # If there are enough seconds for minutes but not hours
        elif (time_in_seconds >= 60) & (time_in_seconds < 60*60):
            minutes = int(time_in_seconds // 60)
            return f"{str(minutes)} minutes {str(round(time_in_seconds - minutes * 60))} seconds"
        # Enough seconds for hours
        else:
            minutes = int(time_in_seconds // 60)
            hours = int(minutes // 60)
            return f"{str(hours)} hours {str(minutes - hours * 60)} minutes " \
                   f"{str(round(time_in_seconds - minutes * 60))} seconds"

    # Basic overview dataframe creation for coverage analysis
    def overview_coverage(coverages):
        # Create overview dataframe, can be changed to suit user need
        # Input:    coverages:  coverage dataframe with samples IDs and coverage in percentages
        # Output:    overview:   dataframe which stores input data created for an easy first look of the data

        # Calculate parameters of interest
        rows = []
        for coverage in "AGORA2_species_coverage", "AGORA2_genus_coverage", "AGORA2_mixed_coverage":
            coverage_of = coverages.loc[coverage]
            mean = coverage_of.mean()
            stdev = coverage_of.std()

            cov_low_099 = len(coverage_of.where(coverage_of < 99).dropna())
            cov_low_09 = len(coverage_of.where(coverage_of < 90).dropna())
            cov_low_05 = len(coverage_of.where(coverage_of < 50).dropna())
            
            row = [coverage.replace(" % ", " "), mean, stdev, cov_low_099, cov_low_09, cov_low_05]
            rows.append(row)
            
        # Create dataframe #TODO is not the real coverage, should be agora coverage i believe
        overview = pd.DataFrame(rows, columns=['Overview', 'Mean Coverage', 'stdev Coverage', 'Coverage <99%',
                                               'Coverage <90%', 'Coverage <50%'])
        return overview

    # #Script# #

    # Initialise the web address where AGORA2 data is stored
    address = "https://marsagora2api.herokuapp.com/agora2/"

    # Initialise the time
    now = datetime.now()
    
    print("Start time: " + str(now.strftime("%H:%M:%S")))
    
    t = time.perf_counter()  # total time recorded
    
    # Creating MARS_output folder if not there already and creates output folder following naming convention
    if not os.path.isdir("MARS_output"):
        os.mkdir("MARS_output")

    path = nextnonexistent("MARS_output/" + output_folder_name)

    if not os.path.isdir(path):
        os.mkdir(path)
    # Just some statements as placeholders to dirty fix the refrence before statement error in lines590-593
    # TODO should we resolve the dirty fix then?
    species_reads_res = ''
    genera_reads_res = ''
    total_reads_res = ''
    # Preprocessing of feature and taxonomy table
    print("Preprocessing...")
    if alt:
        mixed_summed_res, species_summed_res, genera_summed_res, taxonomy_table_res, species_to_genera_dict_res \
            = preprocessing_alt()
    else:
        species_summed_res, species_reads_res, genera_summed_res, genera_reads_res, total_reads_res, \
            taxonomy_table_res, mixed_summed_res, species_to_genera_dict_res = preprocessing()

    # AGORA checking
    print("Checking which species and genera exist in AGORA2...")
    
    # MARS api requests
    response_species = requests.get(address + "species")
    response_genus = requests.get(address + "genus")

    agora2_species = response_species.json()['agora2_species']
    agora2_genera = response_genus.json()['agora2_genera']
    agora2_mixed = agora2_genera + agora2_species
    
    agora2_species_summed, absent_species, present_species = agora_checker(species_summed_res, agora2_species)
    agora2_genera_summed, absent_genera, present_genera = agora_checker(genera_summed_res, agora2_genera)
    agora2_mixed_summed, absent_mixed, genus_to_keep = agora_checker(mixed_summed_res, agora2_mixed, mixed=True,
                                                                     species_to_genus=species_to_genera_dict_res)
    
#     for species in genus_to_keep:
#         agora2_mixed_summed = agora2_mixed_summed.rename(index={species: species.split(None, 1)[0]})
    agora2_mixed_summed = agora2_mixed_summed.rename(index=genus_to_keep)
    
    agora2_mixed_summed = agora2_mixed_summed.groupby(agora2_mixed_summed.index).sum()
    
    # specific taxon reads
    reads_dic_species = taxon_reads(taxonomy_table_res, "species")
    reads_dic_genera = taxon_reads(taxonomy_table_res, "genus")
    
    # Absent/Present
    print("Generating absent/present dataframes...")
    absent_present_species_df = absent_present_df(absent_species, present_species, reads_dic_species)
    absent_present_genera_df = absent_present_df(absent_genera, present_genera, reads_dic_genera)
    
    # Pan and normalize
    print("Normalizing dataframes...")
    agora2_species_normed, agora2_species_reads = pan_normalize(agora2_species_summed)
    agora2_genera_normed, agora2_genera_reads = pan_normalize(agora2_genera_summed)
    agora2_mixed_normed, agora2_mixed_reads = pan_normalize(agora2_mixed_summed)
    
    zero = agora2_species_normed.isin([0]).sum(axis=0)
    microbes = len(agora2_species_normed) - zero
    microbes_frac = microbes/len(agora2_species_normed)

    # Reads dataframe
    print("Building reads file...")
    dfsum = pd.DataFrame()
    if alt:
        dfsum["AGORA2_species_coverage"] = round(agora2_species_reads, 2)
        dfsum["AGORA2_genus_coverage"] = round(agora2_genera_reads, 2)
        dfsum["AGORA2_mixed_coverage"] = round(agora2_mixed_reads, 2)
        dfsum["alpha_diversity"] = microbes
        dfsum["beta_diversity"] = microbes_frac
    else:
        # TODO Should we do something about the warning for total_reads, species_reads and genera_reads?
        dfsum["Total_reads"] = total_reads_res
        dfsum["Species_reads"] = species_reads_res
        dfsum["Genus_reads"] = genera_reads_res
        dfsum["AGORA2_species_reads"] = agora2_species_reads
        dfsum["AGORA2_genus_reads"] = agora2_genera_reads
        dfsum["AGORA2_mixed_reads"] = agora2_mixed_reads
        dfsum["AGORA2_species_coverage"] = round((agora2_species_reads/total_reads_res)*100, 2)
        dfsum["AGORA2_genus_coverage"] = round((agora2_genera_reads/total_reads_res)*100, 2)
        dfsum["AGORA2_mixed_coverage"] = round((agora2_mixed_reads/total_reads_res)*100, 2)
        dfsum["alpha_diversity"] = microbes
        dfsum["beta_diversity"] = microbes_frac
    dfsum = dfsum.T
    
    # Overview creation
    overview_res = overview_coverage(dfsum)
    
    # Tables to csv
    print("Saving files...")
    if not alt:
        taxonomy_table_res.to_csv(path + "/taxonomy_table.csv")
        agora2_species_summed.to_csv(path + "/MARS_species_not_normed.csv")
        agora2_genera_summed.to_csv(path + "/MARS_genus_not_normed.csv")
        agora2_mixed_summed.to_csv(path + "/MARS_mixed_not_normed.csv")
    overview_res.to_csv(path + "/coverage_overview.csv", index=False)
    dfsum.to_csv(path + "/coverage.csv")
    absent_present_species_df.to_csv(path + "/absent_present_species.csv", index=False)
    absent_present_genera_df.to_csv(path + "/absent_present_genera.csv", index=False)
    agora2_species_normed.to_csv(path + "/MARS_species.csv")
    agora2_genera_normed.to_csv(path + "/MARS_genus.csv")
    agora2_mixed_normed.to_csv(path + "/MARS_mixed.csv")

    # If specified by user, run taxa of interest overview function
    if TOI is not None:
        for taxa in TOI:
            taxa_ofinterest_overview(agora2_species_normed, taxa, path, pan_lvl='species')
            taxa_ofinterest_overview(agora2_genera_normed, taxa, path, pan_lvl='genus')

    elapsed_time = time.perf_counter() - t
    print("MARS done! Time elapsed: {}".format(output_time(elapsed_time)))
    
    system = platform.system().lower()
     
    if system == "windows":
        path = path.replace('/', '\\')
        print(f"Files saved to: \"{os.getcwd()}\\{path}\"\n")
    else:
        print(f"Files saved to: \"{os.getcwd()}/path\"\n")

    return agora2_species_normed


# Taxonomic level overview
def taxa_ofinterest_overview(mgpipe_norm_output, taxaOI, path, pan_lvl):
    # Create files that show for each sample the distribution in a user specified taxonomic level
    # Input:    mgpipe_norm_output: panID normed abundance output from MARS used for MgPipe can be either
    #                               species or genus level
    #           taxaOI:             The taxonomic level of interest that the user wants to create an overview of
    #           pan_lvl:            taxonomic level of the panIDs. Can only be Species or Genus!
    # Output:   Saved files with the taxonomic overview for each sample that can be used for figure creation

    # Call the API that creates the panID : taxalevel dictionary for either species or genus pan IDs
    taxa_species_dict = requests.get(r'https://marsagora2api.herokuapp.com/agora2/species_taxa_info').json()
    taxa_genus_dict = requests.get(r'https://marsagora2api.herokuapp.com/agora2/genus_taxa_info').json()

    # Create a dictionary that states a taxonomic level to its corresponding prefix in the agoraAPI
    TaxaLvl_translator = {
        'kingdom': 'k__',
        'phylum': 'p__',
        'class': 'c__',
        'order': 'o__',
        'family': 'f__',
        'genus': 'g__'
    }

    # Obtain the taxonomic level of interest prefix. Use .lower() to improve user experience
    taxa_abbr = TaxaLvl_translator[taxaOI.lower()]

    # Initialise empty dictionary to store {panID:taxonomic level of interest value}
    TOI_dict = {}

    if pan_lvl == 'species':
        for key in taxa_species_dict:
            # Obtain info on all taxonomic levels for a key (species in AGORA2)
            taxa_values = taxa_species_dict[key]
            # Obtain the correct info for the TOI
            TOI_value = taxa_values[
                        taxa_values.index(taxa_abbr) + 3:taxa_values.find(';', taxa_values.index(taxa_abbr))]
            # Alter the species name (key) to be in panID format
            key_panAlter = 'pan' + key.replace(' ', '_')
            # Save in dict
            TOI_dict[key_panAlter] = TOI_value
    elif pan_lvl == 'genus':
        for key in taxa_genus_dict:
            # Obtain info on all taxonomic levels for a key (genus in AGORA2)
            taxa_values = taxa_genus_dict[key]
            # Obtain the correct info for the TOI
            TOI_value = taxa_values[
                        taxa_values.index(taxa_abbr) + 3:taxa_values.find(';', taxa_values.index(taxa_abbr))]
            # Alter the species name (key) to be in panID format
            key_panAlter = 'pan' + key.replace(' ', '_')
            # Save in dict
            TOI_dict[key_panAlter] = TOI_value
    else:
        print('Incorrect input argument for pan_lvl. Please specify species or genus.')
        # TODO Some warning or error because it's not doing anything now?
    # Loop over the agoraAPI dictionary

    # For the abundance file, rename the index (panIDs) to their corresponding TOI classification
    output_TOIalter = mgpipe_norm_output.rename(index=TOI_dict)

    # Sum all the same classifications
    output_grouped = output_TOIalter.groupby(level=0).sum()
    # print(output_grouped)
    output_grouped.to_csv(path + '/TOI_grouped'+taxaOI+pan_lvl+'.csv')
    # Some save statements
    # return


# Created for testing, should not hinder user experience if they import MARS. Delete if necessary
if __name__ == '__main__':

    # output = MARS(r'C:\Users\Bram Nap\Downloads\feature-tableWoLgenome (1).txt',
    #               r'C:\Users\Bram Nap\Downloads\taxonomyWoL (2).tsv', wgs=True, TOI=['Kingdom', 'Phylum'])

    input_file = pd.read_csv(r'C:\Users\Bram Nap\OneDrive - National University of Ireland, Galway\Docs\NAFLD_study\NormalizedAbundancesSorted_nocutoff.csv', index_col=0)

    taxa_ofinterest_overview(input_file, 'phylum', r'C:\Users\Bram Nap\PycharmProjects', 'species')
