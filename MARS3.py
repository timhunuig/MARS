# I think the code will also look less bulky if we have a script where we have all the functions and a different
# script where we call these functions to arrive to different variables like the MgPipe does.


def MARS(): #Some input statements

    feature_table = ''
    taxonomy_table = ''

    #Part we can only do if we have the reads

    def function_that_combines_feature_and_taxanomy_table():

        combined_table = ''
        return combined_table

    def function_that_gets_taxLvl_grouping_on_combined_table(combined_table):
        tbl_phylum = ''
        tbl_genus = ''
        tbl_species = ''

        return tbl_phylum, tbl_genus, tbl_species

    def function_translate_taxa_2AGORA2(species_or_genus):
        #Remove species/genera that are not in agora2

        pan_reads_species = ''
        pan_reads_genera = ''

        return pan_reads_species, pan_reads_genera

    def function_translate_to_TOI_compare_to_raw(tbl_phylum, pan_reads_x):

        # Convert pan_reads to TOI_reads
        TOI_reads = ''
        # Compare tbl_phylum with TOI_reads
        gain_loss_taxa = ''
        return gain_loss_taxa

    # General part

    def function_normalizes_abundances(df):

        #Remove empy columns
        normalized_table = ''

        return normalized_table

    return
