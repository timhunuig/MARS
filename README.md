# MARS
MARS - Microbial Abundances Retrieved from Sequencing data

## Description
Generates input files for mgPipe along with other informative files. 

## Installation
An [Anaconda installation](https://docs.anaconda.com/anaconda/install/index.html) is highly recommended as it is bundled with the python packages needed to run MARS. 
If wanting to use the function inside MATLAB make sure to tick the "Add to path" option during installation. 

## Usage

### MARS(path_to_feature_table, path_to_taxonomy_table, output_folder_name="output", wgs=False, bound=0, alt=False)

MARS requires the path name to the feature table and taxonomy table, respectively, in string format.

If tables are already merged in one file for `path_to_feature_table` type the path to the merged file and `"None"` (with quotations) for the path to  the taxonomy file e.g. `MARS("path/to/merged/file", "None")`

Output will be contained in folder "MARS_output". Output folders inside this can be change to ones liking by modifying output_to_folder_name flag

If data is WGS, wgs flag must `=True` (`true` in MATLAB) or if data is merged and its reads are listed cummatively, the alt flag must `=True` (`true` in MATLAB).

The bound flag may altered, anything below the given bound value in the relative abundance tables will be set to zero. 

## Usage in MATLAB

1. Setting up python environment 

    In most cases 

    ```pe = pyenv```

    is sufficient, however, if multiple versions of python are present one might need to give a specific path e.g.

    ```pe = pyenv('Version','C:\Users\username\anaconda3\python.exe')```

2. Importing MARS

    Make sure you are executing MARS inside the cloned directory or a directory where MARS.py is present. Once so, execute command:

    ```MARS = py.importlib.import_module('MARS');```

3. Execute MARS function

    At this moment I have not found an equivalent to python's allowance of "flag=" in function input in MATLAB. For now make sure flag inputs match the position of where they are in listed function input statement above. 

    ```py.MARS.MARS("path/to/feature/table", "path/to/taxonomy_table", "output_folder_name", false, 0, false)```

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/timhunuig/MARS.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://gitlab.com/timhunuig/MARS/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:89ed2cfd06841ae516f1a65b2ab274c2?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


